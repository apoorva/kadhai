function fitHeight(){
	$('[data-behind]').attr('data-scrollSpeed',-.1).addClass('layer')
	// $('[data-front]').attr('data-scrollSpeed',-1).addClass('layer')
	// $('section').addClass('layer')
	var landing = document.getElementById('landing');
	var menu = document.getElementById('menu');
	landing.style.height = window.innerHeight + 'px';
	// menu.style.top = window.innerHeight + 15 + 'px';
}
window.onresize = function(){
	fitHeight();
}

window.onload = fitHeight
function winY() { return window.pageYOffset}
// function winX() { return {'left':window.pageXOffset, 'right': window.pageXOffset+window.innerWidth}}
var total_scrol = document.documentElement.scrollHeight;
window.onscroll = function(){
	parallax();	
	// setMenu();

}
function elIsVisible(el){
	var top = window.scrollY + window.innerHeight > $(el).offset().top
	var bottom = window.scrollY  < $(el).offset().top + $(el).height()
	return (top && bottom)
}
function parallax(){
	var divs = $('[data-behind]')
	
	// divs = document.getElementsByClassName('layer')
	for( var i=0;i<divs.length;i++){
		if( !elIsVisible(divs[i])) continue;
		// console.log(divs[i])
		var current_scroll = winY() - $(divs[i]).offset().top
		var curr_perc = current_scroll/total_scrol
		var scrollSpeed = parseFloat(divs[i].getAttribute('data-scrollSpeed')) || 0;
		var new_perc = curr_perc * scrollSpeed;
		var final_scroll = new_perc * divs[i].clientHeight;
		// var final_scroll = scrollSpeed * -20
		var backgroundPosition = divs[i].style.backgroundPosition.split(' ')[0]
		if (!backgroundPosition)
			backgroundPosition = window.getComputedStyle(divs[i]).getPropertyValue('background-position').split(' ')[0]
		// window.console.log(backgroundPosition)
		x =  2 * parseInt(final_scroll)+'px'
		// console.log(backgroundPosition + '' +x)
		// var x = $(divs[i]).scrollTop();
		$(divs[i]).css('background-position',backgroundPosition + ' ' + x);
		
		// else
		// 	backgroundPosition = backgroundPosition.split(' ')
		// bgpy = parseInt(backgroundPosition[1].replace('px','')) +  final_scroll
		// divs[i].style["background-position"] = backgroundPosition[0] + ' ' + bgpy + 'px';
	// 	// window.console.log(divs[i].style.top)
	}
}
function setMenu(){
	var menuTop = $('#menu').offset().top
	if (! elementInViewport($('.sections:first')[0])){
		$('#menu').addClass('floating-menu');
	}
	else{
		$('#menu').removeClass('floating-menu')
	}
}
function elementInViewport(el) {
	var eltop = $(el).offset().top + parseInt($(el).parents('.layer').css('top'))
	if(eltop > 0)  return true
	return false
}